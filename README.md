# Prometheus to InfluxDB Pushgateway

## Configuration

```yaml
---
influxdb:
  url: http://influx.example.org:8086
  user: changeme
  pass: changeme
prometheus:
  url: http://prometheus.example.org:9090
queries:

  - prom_query: 'max(irate(node_network_receive_bytes_total{job="node",device="em2"}[5m]) * 8) without (instance)'
    influx_db: sensors
    influx_labels:
      location: Uplink
      name: Downstream
    influx_retention: rp_week
    influx_series: network_traffic

  - prom_query: 'max(irate(node_network_transmit_bytes_total{job="node",device="em2"}[5m]) * 8) without (instance)'
    influx_db: sensors
    influx_labels:
      location: Uplink
      name: Upstream
    influx_retention: rp_week
    influx_series: network_traffic
```

## Run

Put e.g. the following into a cronjob like this:

```cron
* * * * *  nobody  /usr/bin/python3 prometheus2influxdb.py /path/to/config.yaml
```